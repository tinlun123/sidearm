import Vue from 'vue'
import VueRouter from 'vue-router'
import Intro from '../views/Intro.vue'
import Login from '../views/Login.vue'
import Redirect from '../views/Redirect.vue'
import MainApp from '../views/MainApp.vue'

import Home from '../views/Home.vue'
import Cycles from '../views/Cycles.vue'
import Vendors from '../views/Vendors.vue'
import Guides from '../views/Guides.vue'
import Manifest from '../views/Manifest.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Intro',
    component: Intro
  },
  {
    path: '/redirect',
    name: 'Redirect',
    component: Redirect
  },
  {
    path: '/app',
    component: MainApp,
    children: [
      {
        path: '',
        name: 'Sidearm',
        redirect: 'home'
      },
      {
        path: 'home',
        component: Home
      },
      {
        path: 'cycles',
        component: Cycles
      }, 
      {
        path: 'vendors/:vendor?',
        component: Vendors
      }, 
      {
        path: 'guides',
        component: Guides
      },   
      {
        path: '/login',
        component: Login
      },
      {
        path: 'dev/manifest/:link(.*)?',
        component: Manifest
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
