export default {
  created() {
    if (!this.$store.state.auth) {
      console.log("login");
      this.$router.push("/login");
    }
  }
}
