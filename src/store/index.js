import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";
import querystring from "querystring";
import router from "../router";
import Dexie from "dexie";

Vue.use(Vuex);

var db = new Dexie("SidearmDB");
db.version(1).stores({
  manifest: "key"
});

export default new Vuex.Store({
  state: {
    apiHttpHeaders: undefined,
    auth: undefined,
    error: undefined,
    manifest: undefined,
    profile: undefined,
    char: undefined
  },
  mutations: {
    setApiHeaders(state, payload) {
      state.apiHttpHeaders = payload;
    },
    setAuth(state, payload) {
      state.auth = payload;
    },
    showError(state, payload) {
      state.error = payload;
    },
    setManifest(state, payload) {
      state.manifest = payload;
    },
    setProfile(state, payload) {
      localStorage.setItem("profile", JSON.stringify(payload));
      state.profile = payload;
    },
    setChar(state, payload) {
      localStorage.setItem("char", payload);
      state.char = payload;
    }, logout(state) {
      localStorage.removeItem("profile");
      localStorage.removeItem("char");
      localStorage.removeItem("auth");
      state.apiHttpHeaders = undefined;
      state.auth = undefined;
      state.profile = undefined;
      state.char = undefined;
    }
  },
  actions: {
    async createAuth({ commit }, code) {
      let body = {
        grant_type: "authorization_code",
        code: code,
        client_id: process.env.VUE_APP_BUNGIE_ID,
        client_secret: process.env.VUE_APP_BUNGIE_SECRET
      };
      try {
        let resp = await Axios.post(
          "https://www.bungie.net/platform/app/oauth/token/",
          querystring.stringify(body),
          {
            headers: {
              "Content-Type": "application/x-www-form-urlencoded"
            }
          }
        );
        let currentTime = Date.now();
        let auth = {
          access_token: resp.data.access_token,
          access_timeout: resp.data.expires_in * 1000 + currentTime,
          membership_id: resp.data.membership_id,
          refresh_token: resp.data.refresh_token,
          refresh_timeout: resp.data.refresh_expires_in * 1000 + currentTime
        };
        localStorage.setItem("auth", JSON.stringify(auth));
        commit("setApiHeaders", {
          "X-API-Key": process.env.VUE_APP_BUNGIE_KEY,
          Authorization: "Bearer " + auth.access_token
        });
        commit("setAuth", auth);
      } catch (err) {
        console.log(err);
      }
    },
    async initAuth({ commit, dispatch }) {
      let cachedAuth = JSON.parse(localStorage.getItem("auth"));
      if (cachedAuth) {
        commit("setApiHeaders", {
          "X-API-Key": process.env.VUE_APP_BUNGIE_KEY,
          Authorization: "Bearer " + cachedAuth.access_token
        });
        commit("setAuth", cachedAuth);
        await dispatch("refreshAuth");
      }
    },
    async refreshAuth({ commit, state }) {
      if (state.auth && state.auth.refresh_timeout - 60000 * 10 > Date.now()) {
        if (state.auth.access_timeout < Date.now()) {
          let body = {
            grant_type: "refresh_token",
            refresh_token: state.auth.refresh_token,
            client_id: process.env.VUE_APP_BUNGIE_ID,
            client_secret: process.env.VUE_APP_BUNGIE_SECRET
          };
          let resp = await Axios.post(
            "https://www.bungie.net/platform/app/oauth/token/",
            querystring.stringify(body),
            {
              headers: {
                "Content-Type": "application/x-www-form-urlencoded"
              }
            }
          );
          let currentTime = Date.now();
          let auth = {
            access_token: resp.data.access_token,
            access_timeout: resp.data.expires_in * 1000 + currentTime,
            membership_id: resp.data.membership_id,
            refresh_token: resp.data.refresh_token,
            refresh_timeout: resp.data.refresh_expires_in * 1000 + currentTime
          };
          localStorage.setItem("auth", JSON.stringify(auth));
          commit("setApiHeaders", {
            "X-API-Key": process.env.VUE_APP_BUNGIE_KEY,
            Authorization: "Bearer " + auth.access_token
          });
          commit("setAuth", auth);
        }
      } else {
        router.push("/login");
      }
    },
    async updateManifest({ commit, state }) {
      let manifestCache = (
        await db.manifest
          .where("key")
          .equalsIgnoreCase("manifest")
          .toArray()
      )[0];

      let manifestInfo = await Axios.get(
        "https://www.bungie.net/Platform/Destiny2/Manifest/",
        { headers: state.apiHttpHeaders }
      );
      if (
        manifestCache &&
        manifestCache.version == manifestInfo.data.Response.version
      ) {
        commit(
          "setManifest",
          Object.freeze(JSON.parse(await manifestCache.cache.text()))
        );
      } else {
        let newManifest = (
          await Axios.get(
            "https://www.bungie.net" +
            manifestInfo.data.Response.jsonWorldContentPaths.en,
            { responseType: "blob" }
          )
        ).data;
        await db.manifest.put({
          key: "manifest",
          version: manifestInfo.data.Response.version,
          cache: newManifest
        });
        commit(
          "setManifest",
          Object.freeze(JSON.parse(await newManifest.text()))
        );
      }
    },
    async initProfiles({ commit, state }) {
      if (!state.auth) {
        return;
      }
      let profileCache = JSON.parse(localStorage.getItem("profile"));
      let bungieAccount = await Axios.get(
        "https://www.bungie.net/Platform/User/GetMembershipsById/" +
        state.auth.membership_id +
        "/254/",
        { headers: state.apiHttpHeaders }
      );
      if (
        profileCache &&
        bungieAccount.data.Response.destinyMemberships.some(
          e => e.membershipId == profileCache.membershipId
        )
      ) {
        commit("setProfile", profileCache);
      } else {
        let newProfile;
        if (bungieAccount.data.Response.primaryMembershipId) {
          newProfile = {
            membershipId: bungieAccount.data.Response.primaryMembershipId,
            membershipType: bungieAccount.data.Response.destinyMemberships.find(membership => membership.membershipId == bungieAccount.data.Response.primaryMembershipId).membershipType
          }
        } else {
          newProfile = {
            membershipId: bungieAccount.data.Response.destinyMemberships[0].membershipId,
            membershipType: bungieAccount.data.Response.destinyMemberships[0].membershipType
          }
        }
        commit("setProfile", newProfile);
      }
      let charCache = localStorage.getItem("char");
      let accountChars = await Axios.get(
        "https://www.bungie.net/Platform/Destiny2/" +
        state.profile.membershipType +
        "/Profile/" +
        state.profile.membershipId +
        "/?components=200",
        { headers: state.apiHttpHeaders }
      );
      if (
        charCache &&
        accountChars.data.Response.characters.data[accountChars]
      ) {
        commit("setChar", charCache);
      } else {
        let newChar = Object.keys(
          accountChars.data.Response.characters.data
        )[0];
        commit("setChar", newChar);
      }
      return {
        profileOpts: bungieAccount.data.Response.destinyMemberships,
        charOpts: Object.keys(accountChars.data.Response.characters.data).map(
          k => accountChars.data.Response.characters.data[k]
        ),
        primaryProfile: bungieAccount.data.Response.primaryMembershipId
      };
    },
    async updateProf({ commit, state }, prof) {
      commit("setProfile", prof);
      let accountChars = await Axios.get(
        "https://www.bungie.net/Platform/Destiny2/" +
        state.profile.membershipType +
        "/Profile/" +
        state.profile.membershipId +
        "/?components=200",
        { headers: state.apiHttpHeaders }
      );
      if (!accountChars.data.Response.characters.data[state.char]) {
        commit("setChar", accountChars.data.Response.characters.data[0]);
      }
      return Object.keys(accountChars.data.Response.characters.data).map(
        k => accountChars.data.Response.characters.data[k]
      );
    }
  },
  modules: {}
});
